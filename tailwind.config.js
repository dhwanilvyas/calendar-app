module.exports = {
  purge: [],
  theme: {
    extend: {
      top: {
        "1/2": "50%"
      },
      left: {
        "1/2": "50%"
      },
      transitionProperty: {
        height: "height"
      }
    }
  },
  variants: {},
  plugins: []
};
