import React, { useState } from "react";
import {
  getDay,
  isToday,
  isWeekend,
  isThisWeek,
  format,
  isSameDay,
  isAfter
} from "date-fns";
import { X } from "heroicons-react";
import { getDayName } from "../../utils/utils";
import AddReminder from "../AddReminder/AddReminder";
import Reminders from "../Reminders/Reminders";

const DayView = ({
  isFullDayViewOpen,
  openFullView,
  closeFullView,
  selectedDay
}) => {
  const [isAddReminderOpen, setAddReminderOpen] = useState(false);
  const day = getDayName(getDay(selectedDay));

  return (
    <div
      className={`h-full rounded-b overflow-y-auto ${
        isWeekend(selectedDay) ? "bg-green-500" : "bg-blue-500"
      } p-4 px-8`}
    >
      {isFullDayViewOpen ? (
        <div className="mb-4 flex flex-row justify-end items-center">
          <button
            onClick={e => {
              e.persist();
              e.stopPropagation();
              closeFullView();
            }}
          >
            <X color="white" size={20} />
          </button>
        </div>
      ) : null}
      <div className="mb-6 flex flex-row justify-between items-center">
        <div>
          <h1 className="text-white">
            {isToday(selectedDay)
              ? "Today"
              : isThisWeek(selectedDay)
              ? `This ${day}`
              : day}
          </h1>
          <p className="text-white text-xs">
            {format(selectedDay, "dd/MM/yyyy")}
          </p>
        </div>
        {isSameDay(selectedDay, new Date()) ||
        isAfter(selectedDay, new Date()) ? (
          <button
            className="text-white"
            onClick={e => {
              e.persist();
              e.stopPropagation();
              setAddReminderOpen(true);
            }}
          >
            Add New
          </button>
        ) : null}
      </div>
      <div onClick={openFullView}>
        <Reminders
          isFullDayViewOpen={isFullDayViewOpen}
          selectedDay={selectedDay}
        />
      </div>
      {isAddReminderOpen ? (
        <AddReminder
          onClose={() => setAddReminderOpen(false)}
          selectedDay={selectedDay}
        />
      ) : null}
    </div>
  );
};

export default DayView;
