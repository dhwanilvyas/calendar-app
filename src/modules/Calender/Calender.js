import React, { useState } from "react";
import DayPicker from "react-day-picker";
import { isWeekend, isSameMonth, isSameDay, isToday } from "date-fns";
import Navigation from "./Navigation";
import "react-day-picker/lib/style.css";
import SelectMonthYear from "./SelectMonthYear";
import DayView from "./DayView";
import { getWeekDayInitials, getRemindersByDay } from "../../utils/utils";
import storeHouse from "../../utils/storeHouse";

const WeekDays = ({ className, weekday }) => {
  const arr = getWeekDayInitials();
  const isWeekEnd = weekday === 0 || weekday === 6;

  return (
    <div
      className={`${className}`}
      style={{ color: isWeekEnd ? "green" : "black" }}
    >
      {arr[weekday]}
    </div>
  );
};

const renderDay = (day, selectedDay) => {
  const circleColor =
    isSameDay(day, selectedDay) && isWeekend(day)
      ? "green-600"
      : isSameDay(day, selectedDay)
      ? "blue-600"
      : "white";

  const textColor = isSameDay(day, selectedDay)
    ? "white"
    : isWeekend(day)
    ? "green-600"
    : "blue-600";

  const circleBorderColor =
    !isSameDay(day, selectedDay) && isToday(day)
      ? isWeekend(day)
        ? "border-green-600"
        : "border-blue-600"
      : "";

  let reminders = getRemindersByDay(storeHouse.getReminders(), day);
  let dotColor = isSameDay(day, selectedDay)
    ? "white"
    : isWeekend(day)
    ? "green-600"
    : "blue-600";
  if (reminders.length > 3) {
    reminders = reminders.slice(0, 3);

    if (isSameDay(day, selectedDay)) {
      dotColor = "white";
    } else {
      dotColor = "red-600";
    }
  }

  return (
    <div
      className={`p-2 bg-${circleColor} ${
        circleBorderColor ? "border-2" : ""
      } ${circleBorderColor} rounded-full flex flex-col items-center`}
    >
      <span className={`text-${textColor}`}>{day.getDate()}</span>
      {reminders.length ? (
        <div className="flex flex-row">
          {reminders.map((reminder, index) => (
            <span
              key={reminder.id}
              className={`relative rounded-full h-1 w-1 bg-${dotColor} ${
                index === 1 ? "ml-1 mr-1" : ""
              }`}
            ></span>
          ))}
        </div>
      ) : null}
    </div>
  );
};

const Calender = () => {
  const [isOpenSelectMonthYear, setSelectMonthYearOpen] = useState(false);
  const [selectedDay, setSeletedDay] = useState(new Date());
  const [isFullDayViewOpen, setFullDayViewOpen] = useState(false);

  const onMonthClick = () => setSelectMonthYearOpen(true);

  const onSelect = date => {
    setSeletedDay(date);
    setSelectMonthYearOpen(false);
  };

  const onDayClick = date => {
    setSeletedDay(date);
  };

  const onMonthChange = date => {
    if (isSameMonth(date, new Date())) {
      setSeletedDay(new Date());
    } else {
      setSeletedDay(date);
    }
  };

  return (
    <div className="container h-full flex flex-col sm:w-full md:w-full lg:w-1/3 m-auto md:p-4">
      <div
        className="flex justify-center bg-white items-center rounded overflow-hidden shadow-lg"
        style={{ height: "60%" }}
      >
        <DayPicker
          todayButton={
            !isSameMonth(new Date(), selectedDay) ? "GO TO TODAY'S DATE" : null
          }
          navbarElement={<Navigation onMonthClick={onMonthClick} />}
          captionElement={<span />}
          weekdayElement={<WeekDays />}
          month={selectedDay}
          renderDay={day => renderDay(day, selectedDay)}
          selectedDays={[selectedDay]}
          onDayClick={onDayClick}
          onMonthChange={onMonthChange}
        />
      </div>
      <div
        className="cursor-pointer transition transition-height duration-300 ease-linear"
        style={{ height: isFullDayViewOpen ? "100vh" : "40%" }}
      >
        <DayView
          selectedDay={selectedDay}
          isFullDayViewOpen={isFullDayViewOpen}
          openFullView={() => setFullDayViewOpen(true)}
          closeFullView={() => setFullDayViewOpen(false)}
        />
      </div>
      {isOpenSelectMonthYear ? (
        <SelectMonthYear
          onSelect={onSelect}
          onClose={() => setSelectMonthYearOpen(false)}
        />
      ) : null}
    </div>
  );
};

export default Calender;
