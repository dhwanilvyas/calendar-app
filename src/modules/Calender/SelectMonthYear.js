import React, { useRef } from "react";
import { LocaleUtils } from "react-day-picker";
import { ChevronDownOutline } from "heroicons-react";
import Modal from "../../components/Modal/Modal";
import { getYearRange } from "../../utils/utils";

const months = LocaleUtils.getMonths();

const SelectMonthYear = ({ onClose, onSelect: propOnSelect }) => {
  const year = useRef();
  const month = useRef();

  const onSelect = () => {
    propOnSelect(
      new Date(year.current.value, months.indexOf(month.current.value), 1)
    );
  };

  return (
    <Modal title="Select Month & Year" onClose={onClose}>
      <div className="p-4">
        <div className="mb-8 inline-block relative w-full">
          <select
            ref={month}
            className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline"
          >
            {months.map(month => (
              <option key={month}>{month}</option>
            ))}
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <ChevronDownOutline size={15} />
          </div>
        </div>
        <div className="mb-8 inline-block relative w-full">
          <select
            ref={year}
            className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline"
          >
            {getYearRange().map(year => (
              <option key={year}>{year}</option>
            ))}
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <ChevronDownOutline size={15} />
          </div>
        </div>
      </div>
      <button
        onClick={onSelect}
        className="w-full absolute bottom-0 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
      >
        Select
      </button>
    </Modal>
  );
};

export default SelectMonthYear;
