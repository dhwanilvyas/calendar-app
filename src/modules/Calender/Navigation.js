import React from "react";
import dateFnsFormat from "date-fns/format";
import { ChevronLeft, ChevronRight } from "heroicons-react";

const Navigation = ({ onPreviousClick, month, onNextClick, onMonthClick }) => {
  return (
    <div className="m-auto w-11/12 flex flex-row justify-between">
      <ChevronLeft
        className="cursor-pointer"
        color="grey"
        onClick={() => onPreviousClick()}
      />
      <h1
        className="text-orange-500 uppercase cursor-pointer"
        onClick={() => onMonthClick()}
      >
        {dateFnsFormat(month, "MMMM Y")}
      </h1>
      <ChevronRight
        color="grey"
        className="cursor-pointer"
        onClick={() => onNextClick()}
      />
    </div>
  );
};

export default Navigation;
