import React, { useState } from "react";
import { format } from "date-fns";
import randomId from "randomid";
import { ChevronDownOutline } from "heroicons-react";
import Modal from "../../components/Modal/Modal";
import storeHouse from "../../utils/storeHouse";
import { getTimeRange } from "../../utils/utils";
import { useToast } from "../../components/Toast/ToastContext";

const AddEvent = ({ onClose, selectedDay }) => {
  const toast = useToast();

  const timeRange = getTimeRange({ start: "00:00", end: "20:00" });
  const [formData, setFormData] = useState({
    title: "",
    date: format(selectedDay, "yyyy-MM-dd"),
    fromTime: timeRange[0],
    toTime: timeRange[1],
    location: ""
  });

  const save = () => {
    const { title, date, location, fromTime, toTime } = formData;
    storeHouse.setReminder({
      id: randomId(),
      title,
      date,
      location,
      fromTime,
      toTime
    });
    toast.show({ content: "Reminder added" });
    onClose();
  };

  const shouldDisable = () =>
    Object.values(formData).some(value => value.length === 0);

  const handleInputChange = e => {
    e.persist();
    setFormData(data => ({
      ...data,
      [e.target.name]: e.target.value
    }));
  };

  return (
    <Modal title="Add Reminder" onClose={onClose}>
      <div className="p-4 transition duration-500 ease-linear">
        <label htmlFor="title">Reminder Title</label>
        <input
          name="title"
          className="mt-1 mb-4 bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
          type="text"
          placeholder="Coffee chat with Elon Musk"
          value={formData.title}
          onChange={handleInputChange}
        />
        <label htmlFor="date">Date</label>
        <input
          name="date"
          className="mt-1 mb-4 bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
          type="date"
          placeholder="Coffee chat with Elon Musk"
          defaultValue={format(selectedDay, "yyyy-MM-dd")}
          onChange={handleInputChange}
        />
        <label htmlFor="location">Location</label>
        <input
          name="location"
          className="mt-1 mb-4 bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
          type="text"
          placeholder="CCD"
          onChange={handleInputChange}
        />
        <div className="mb-4 inline-block relative w-full">
          <label htmlFor="fromTime">From time</label>
          <select
            name="fromTime"
            className="mt-1 mb-4 block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline"
            onChange={handleInputChange}
          >
            {timeRange.map(month => (
              <option key={month}>{month}</option>
            ))}
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <ChevronDownOutline size={15} />
          </div>
        </div>
        <div className="mb-4 inline-block relative w-full">
          <label htmlFor="toTime">To time</label>
          <select
            name="toTime"
            className="mt-1 mb-4 block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline"
            onChange={handleInputChange}
          >
            {timeRange.map(month => (
              <option key={month}>{month}</option>
            ))}
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <ChevronDownOutline size={15} />
          </div>
        </div>
      </div>
      <button
        disabled={shouldDisable()}
        className={`w-full fixed bottom-0 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ${
          shouldDisable() ? "opacity-50 cursor-not-allowed" : ""
        }`}
        onClick={save}
      >
        Save
      </button>
    </Modal>
  );
};

export default AddEvent;
