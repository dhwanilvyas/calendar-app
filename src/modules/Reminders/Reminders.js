import React from "react";
import { LocationMarker, Clock } from "heroicons-react";
import { isWeekend } from "date-fns";
import storeHouse from "../../utils/storeHouse";
import { getRemindersByDay } from "../../utils/utils";
import "./Reminders.css";

const Reminders = ({ selectedDay }) => {
  const reminders = getRemindersByDay(storeHouse.getReminders(), selectedDay);

  if (!reminders.length) {
    return <h1 className="h-full mt-8 text-center text-white">No reminders</h1>;
  }

  return (
    <div className="h-full">
      <ul className="mt-4 list-none">
        <React.Fragment>
          {reminders.map(reminder => (
            <li
              key={reminder.id}
              className={isWeekend(selectedDay) ? "green" : "blue"}
            >
              <h1 className="text-white font-bold">{reminder.title}</h1>
              <div className="mt-1 text-xs font-light text-white flex flex-row items-center">
                <LocationMarker size={15} />
                <p className="ml-1">{reminder.location}</p>
              </div>
              <div className="mt-2 text-xs font-light text-white flex flex-row items-center">
                <Clock size={15} />
                <p className="ml-1">
                  {reminder.fromTime} - {reminder.toTime || "N/A"}
                </p>
              </div>
            </li>
          ))}
        </React.Fragment>
      </ul>
    </div>
  );
};

export default Reminders;
