import React from "react";

const ToastContext = React.createContext();

const useToast = () => React.useContext(ToastContext);

export { useToast };

export default ToastContext;
