import React, { useState } from "react";
import ReactDOM from "react-dom";
import ToastContext from "./ToastContext";

const ToastProvider = ({ children }) => {
  const [toast, setToast] = useState(null);
  const toastRef = React.useRef(document.createElement("div"));
  toastRef.current.id = "toast-root";
  toastRef.current.className = "relative sm:w-full md:w-1/3 m-auto";

  React.useEffect(() => {
    document.body.appendChild(toastRef.current);
    const modal = toastRef.current;
    return () => document.body.removeChild(modal);
  }, []);

  React.useEffect(() => {
    let id = null;
    if (toast) {
      id = setTimeout(() => setToast(null), 2000);
    }

    return () => clearTimeout(id);
  }, [toast]);

  const renderToast = () => {
    if (!toast) return null;
    return (
      <div className="absolute md:mx-4 bg-green-400 p-4 z-50 left-0 right-0 bottom-0">
        <p className="text-white">{toast.content}</p>
      </div>
    );
  };

  return (
    <ToastContext.Provider
      value={{
        show: toast => setToast(toast)
      }}
    >
      {children}
      {ReactDOM.createPortal(renderToast(), toastRef.current)}
    </ToastContext.Provider>
  );
};

export default ToastProvider;
