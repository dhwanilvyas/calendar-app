import React, { useRef } from "react";
import { X } from "heroicons-react";
import ReactDOM from "react-dom";

const Modal = ({ children, onClose, title }) => {
  const modalRef = useRef(document.createElement("div"));
  modalRef.current.id = "modal-root";

  React.useEffect(() => {
    document.body.appendChild(modalRef.current);
    const modal = modalRef.current;
    return () => document.body.removeChild(modal);
  }, []);

  return ReactDOM.createPortal(
    <div className="fixed inset-0 bg-white md:w-1/3 m-auto overflow-y-scroll">
      <React.Fragment>
        <div className="h-16 p-4 flex flex-row justify-between items-center">
          <p className="text-xl">{title}</p>
          <button onClick={onClose}>
            <X color="grey" size={20} />
          </button>
        </div>
        {children}
      </React.Fragment>
    </div>,
    modalRef.current
  );
};

export default Modal;
