import React from "react";
import Calender from "./modules/Calender/Calender";
import "./App.css";
import ToastProvider from "./components/Toast/ToastProvider";

function App() {
  return (
    <div className="App h-full">
      <ToastProvider>
        <Calender />
      </ToastProvider>
    </div>
  );
}

export default App;
