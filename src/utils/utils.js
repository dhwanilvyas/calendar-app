import { addMinutes, format as formatDate, isSameDay } from "date-fns";

const weeDayNames = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday"
];

export const getDayName = day => weeDayNames[day];

export const getWeekDayInitials = () => weeDayNames.map(day => day.charAt(0));

export const getYearRange = () => {
  const currentYear = new Date().getFullYear();
  const fromYear = currentYear - 10;
  const toYear = currentYear + 10;
  const years = Array.from(
    new Array(toYear - fromYear),
    (val, index) => index + fromYear
  );

  return years;
};

export const getTimeRange = ({
  start = "00:00",
  end = "23:00",
  difference = 60,
  format = false
}) => {
  const times = [];
  let currentTime = null;
  let startDate = new Date();

  const formatCurrentTime = () => {
    currentTime = format
      ? formatDate(startDate, "hh:mm A")
      : formatDate(startDate, "HH:mm");
  };

  const areSame = () => {
    let hours = startDate.getHours();
    let minutes = startDate.getMinutes();

    hours = hours < 10 ? `0${hours}` : hours;
    minutes = minutes < 10 ? `0${minutes}` : minutes;

    return `${hours}:${minutes}` === end;
  };

  const [startTime, endTime] = start.split(":");
  startDate.setHours(+startTime);
  startDate.setMinutes(+endTime);

  formatCurrentTime();
  times.push(currentTime);

  while (!areSame()) {
    startDate = addMinutes(startDate, difference);
    formatCurrentTime();
    times.push(currentTime);
  }

  return times;
};

export default getTimeRange;

export const getRemindersByDay = (reminders, selectedDay) =>
  reminders.filter(reminder => isSameDay(selectedDay, new Date(reminder.date)));
