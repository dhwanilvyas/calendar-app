const storeHouse = {
  getReminders() {
    const reminders = localStorage.getItem("reminders");
    return reminders ? JSON.parse(reminders) : [];
  },
  setReminder(reminder) {
    const reminders = this.getReminders("reminders");
    localStorage.setItem("reminders", JSON.stringify([...reminders, reminder]));
  }
};

export default storeHouse;
